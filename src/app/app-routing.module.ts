import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GroupsComponent} from './groups/groups.component';
import {PayrollsComponent} from './payrolls/payrolls.component';
import {HomeComponent} from './home/home.component';
import {InviteComponent} from './invite/invite.component';
import {VerifyComponent} from './verify/verify.component';

const routes: Routes = [
  { path: 'groups', component: GroupsComponent },
  { path: 'payrolls', component: PayrollsComponent},
  { path: 'invite', component: InviteComponent},
  { path: 'verify', component: VerifyComponent},
  { path: 'home', component: HomeComponent},
  { path: '', pathMatch: 'full', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
